# Azur Lane Weapon Tier List
###### Last updated: 6/2/2020
###### All images taken from [Azur Lane Wiki](https://azurlane.koumakan.jp/)
Below is v1.0 of the weapon tier list. Initial layout and data taken from [here](https://i.redd.it/nz4b9mqcl6m31.png)


### Table of Contents
1. [DD Section](#dd-section)
2. [CL Section](#cl-section)
3. [CA Section](#ca-section)
4. [BB Section](#bb-section)
5. [Fighers](#fighters)
6. [Dive Bombers](#dive-bombers)
7. [Torpedo Bombers](#torpedo-bombers)
8. [Torpedos](#torpedos)
9. [Submarine Torpedos](#submarine-torpedos)
10. [AA Section](#aa-section)
11. [Auxiliary](#auxiliary)
12. [Uniques](#uniques)

## DD Section
### DD Weapons - Damage Specialization

<table>
<tr>
    <th>Rank</th> <th>Weapon Tier</th> <th>Name</th> <th>Picture</th>
</tr>
<tr>
    <td>1</td> <td>T3</td> <td><a href="https://azurlane.koumakan.jp/Single_138.6mm_(Mle_1929)#Type_3">Single 138.6</a></td> <td>![Single 138.6](https://azurlane.koumakan.jp/w/images/b/bc/50100.png)</td>
</tr>
</table>

### DD Weapons - Barrage Specialization

<table>
<tr>
    <th>Rank</th> <th>Weapon Tier</th> <th>Name</th> <th>Picture</th>
</tr>
<tr>
    <td>1</td> <td>T3</td> <td><a href="https://azurlane.koumakan.jp/Twin_100mm_(Type_98)#Type_3">Twin 100 (Type 98)</a></td> <td>![Twin 100 (Type 98)](https://azurlane.koumakan.jp/w/images/thumb/3/3d/31000.png/128px-31000.png)</td>
</tr>
</table>

### DD Weapons - Auxiliary

<table>
<tr>
    <th>Rank</th> <th>Weapon Tier</th> <th>Name</th> <th>Picture</th>
</tr>
<tr>
    <td>1</td> <td>T3</td> <td><a href="https://azurlane.koumakan.jp/Type_93_Pure_Oxygen_Torpedo#Type_3">Type 93 Pure Oxygen Torpedo</a></td> <td>![Type 93 Pure Oxygen Torpedo](https://azurlane.koumakan.jp/w/images/thumb/5/59/2600.png/128px-2600.png)</td>
</tr>
</table>

## CL Section

## CA Section

## BB Section

## Fighters

## Dive Bombers

## Torpedo Bombers

## Torpedos

## Submarine Torpedos

## AA Setion

## Auxiliary

## Uniques
