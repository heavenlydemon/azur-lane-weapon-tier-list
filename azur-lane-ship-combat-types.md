# Azur Lane Ship Combat Types
###### Last updated: 6/2/2020
###### All images taken from [Azur Lane Wiki](https://azurlane.koumakan.jp/)

Below is v1.0 of the ship combat types. This is meant to clarify what sub-category of combat each ship is.

### Table of Contents
1. [DD Section](#dd-section)
2. [CL Section](#cl-section)
3. [CA Section](#ca-section)
4. [BB Section](#bb-section)

## DD Section
<table>
<tr>
    <th>Name</th>
    <th>Combat Type</th>
    <th>Picture</th>
</tr>
<tr>
    <td>Z23</td>
    <td>Damage</td>
    <td>![Z23](https://azurlane.koumakan.jp/w/images/8/86/Z23Icon.png)</td>
</tr>
</table>


## CL Section
<table>
<tr>
    <th>Name</th>
    <th>Combat Type</th>
    <th>Picture</th>
</tr>
<tr>
    <td>Leander</td>
    <td>Damage</td>
    <td>![Leander](https://azurlane.koumakan.jp/w/images/7/7f/LeanderIcon.png)</td>
</tr>
</table>

## CA Section
<table>
<tr>
    <th>Name</th>
    <th>Combat Type</th>
    <th>Picture</th>
</tr>
<tr>
    <td>London</td>
    <td>Damage</td>
    <td>![London](https://azurlane.koumakan.jp/w/images/1/11/LondonIcon.png)</td>
</tr>
</table>

## BB Section
<table>
<tr>
    <th>Name</th>
    <th>Combat Type</th>
    <th>Picture</th>
</tr>
<tr>
    <td>Hood</td>
    <td>Barrage</td>
    <td>![Hood](https://azurlane.koumakan.jp/w/images/0/00/HoodIcon.png)</td>
</tr>
</table>
